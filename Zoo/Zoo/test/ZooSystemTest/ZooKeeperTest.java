package ZooSystemTest;

import ZooSystem.Admin;
import ZooSystem.Doctor;
import ZooSystem.Employee;
import ZooSystem.ZooKeeper;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZooKeeperTest {
    @Test
    public void test_SetSalary(){
        Admin zk = new Admin("Jamie");
        zk.setSalary(100);
        //assertEquals(100, zk.getSalary());
        assertEquals(90, zk.getSalary());
    }
    @Test
    public void test_EmployeeName() {
        Admin zk = new Admin("allen");
        assertEquals("Allen", zk.getEmployeeName());
    }
}
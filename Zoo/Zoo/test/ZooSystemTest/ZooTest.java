package ZooSystemTest;

import ZooSystem.Zoo;
import org.junit.Test;

import static org.junit.Assert.*;

public class ZooTest {
    @Test
    public void test_Zoo() {
        Zoo z1 = new Zoo();
        assertTrue(30==Zoo.getNumCompounds());
    }

    @Test
    public void test_numCompounds() {
        Zoo z1 = new Zoo("Paris", 50);
        assertEquals(50, z1.getNumCompounds());
        //assertEquals(51, z1.getNumCompounds());

    }

}
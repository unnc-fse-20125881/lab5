package ZooSystem;

public class Doctor extends ZooKeeper {
    private String emergencyPhoneNumber;
    public Doctor(String name) {
        super(name);
    }

    public void setEmergencyPhoneNumber(String phone) {
        char[] ch = phone.toCharArray();
        if (ch[0] == 0) {
            if (ch.length == 11) {
                this.emergencyPhoneNumber = phone;
            }
        }else {
            this.emergencyPhoneNumber = null;
        }
    }

    public String getEmergencyPhoneNumber() {
        return emergencyPhoneNumber;
    }

}

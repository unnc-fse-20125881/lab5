package ZooSystem;

import java.util.ArrayList;

public class ZooCorp {
    private ArrayList<Zoo> zoos;
    private ArrayList<Employable> personnel;

    public ZooCorp(Zoo zoo) {
        this.zoos = new ArrayList<Zoo>();
        personnel = new ArrayList<Employable>();
        addZoo(zoo);
    }

    public void addZoo(Zoo zoo) {
        this.zoos.add(zoo);
    }
    public void addStaff(Employable person) {
        personnel.add(person);
    }
}

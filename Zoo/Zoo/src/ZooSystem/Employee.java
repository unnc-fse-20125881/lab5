package ZooSystem;

public abstract class Employee implements Employable {
    private int ID;
    private String name;
    private int salary;

	public Employee(String name) {
		setEmployeeName(name);
	}

    @Override
    public void setEmployeeID(int number) {
        ID = number; // "this." not needed as ID and number have different names
    }

    @Override
    public int getEmployeeID() {
        return ID;
    }

    @Override
    public void setEmployeeName(String name) {
        String firstCap = name.substring(0,1).toUpperCase();
        String latter = name.substring(1);
        String changed_name = firstCap + latter;

	    this.name = changed_name;
    }

    @Override
    public String getEmployeeName() {
        return name;
    }

    @Override
    public void setSalary(int salary) {
        this.salary = (int)(salary * 0.9);
    }

    @Override
    public int getSalary() {
        return salary;
    }
}

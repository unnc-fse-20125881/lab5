package ZooSystem;

import java.util.ArrayList;

public class Zoo {
    //private fields
    private String location;
    private static int numCompounds;
    public static int numZoos;
    private int zooID;
    private ArrayList<Compound> compounds;

    public Zoo(String location, int numCompounds) {   //constructor #1
        setLocation(location);
        setNumCompounds(numCompounds);
        compounds = new ArrayList<Compound>();
        numZoos++;
        zooID = numZoos;
        for (int i=0; i<numCompounds; i++){
            addCompounds(new Compound());
        }
    }

    private void addCompounds(Compound compound) {
        this.compounds.add(compound);
    }

    public Zoo(){                   //constructor #2
        this("Unknown", 30);
    }

    public void buildNewCompound(){
        this.numCompounds++;
    }

    public static int getNumCompounds() {
        return numCompounds;
    }

    public void setNumCompounds(int numCompounds) {
        this.numCompounds = numCompounds;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void printInfo() {
        System.out.println("location:" + location + "; numCompounds:" + numCompounds);
    }
}
